#include <Arduino.h>
#include <HardwareSerial.h>

//MIDI REFERENCE: https://www.midi.org/specifications-old/item/table-1-summary-of-midi-message

/* Channel Voice Messages [nnnn = 0-15 (MIDI Channel Number 1-16)] */
#define CMD_NOTE_ON   B10010000 // KEY, VELO
#define CMD_NOTE_OFF  B10000000 // KEY, VELO
#define CMD_POSITION  B11110010 // This is an internal 14 bit register that holds the number of MIDI beats
                                // (1 beat= six MIDI clocks) since the start of the song. l is the LSB, m the MSB.
#define CMD_CONTROL   B10110010 // 
/* System Real-Time Messages */
#define CMD_STOP      B11111100 // Stop the current sequence.
#define CMD_START     B11111010 // Start the current sequence playing.
                                            // (This message will be followed with Timing Clocks).
#define CMD_CONTINUE  B11111011 // Continue at the point the sequence was Stopped


/*
 * Functions to send midi commands and paramtersa as bytes over serial
 * Serial messages should be handled by software to convert it MIDI signal for the OS
 * This project uses 
 * loopmidi: https://www.tobias-erichsen.de/software/loopmidi.html
 * Hairless midi: https://www.softpedia.com/get/System/System-Miscellaneous/Hairless-MIDI-to-Serial-Bridge.shtml
  */

void MIDIMessage(int8_t command, int8_t data1, int8_t data2){
  Serial.write(command);
  Serial.write(data1);
  Serial.write(data2);
}
void MIDIMessage(int8_t command, int8_t data1){
  Serial.write(command);
  Serial.write(data1);
}
void MIDIMessage(int8_t command){
  Serial.write(command);
}
